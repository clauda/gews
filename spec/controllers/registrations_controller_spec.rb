require 'rails_helper'

RSpec.describe RegistrationsController, type: :controller do
  let(:subject) { FactoryGirl.create :registration }
  
  let(:user) { FactoryGirl.create :user }
  let(:valid_session) { sign_in user }

  describe "DELETE destroy" do
    before { request.env["HTTP_REFERER"] = "back" }

    it "confirm registration presence" do
      expect {
        post :confirm, { id: subject.id, field: 'confirm' }, valid_session
      }.to change(Registration.confirmed, :size).by(1)
    end

    it "destroys the requested registration" do
      expect {
        delete :destroy, { id: subject.to_param }, valid_session
      }.to change(Registration, :count).by(0)
    end

    it "redirects to the event view" do
      delete :destroy, { id: subject.to_param }, valid_session
      expect(response).to redirect_to("back")
    end

  end

end
