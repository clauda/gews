require 'rails_helper'

RSpec.describe ParticipantsController, type: :controller do
  let(:user) { FactoryGirl.create :user }
  let(:subject) { FactoryGirl.create :participant }

  let(:valid_attributes) { { name: "Lorem Ipsum" } }
  let(:invalid_attributes) { { name: nil, city: nil, state: nil } }
  let(:valid_session) { sign_in user }

  describe "GETs" do

    describe "GET index" do
      it "assigns all participants as @participants" do
        get :index, {}, valid_session
        expect(assigns(:participants)).to eq([subject])
      end
    end

    describe "GET show" do
      it "assigns the requested participant as @participant" do
        get :show, { id: subject.to_param }, valid_session
        expect(assigns(:participant)).to eq(subject)
      end
    end

    describe "GET new" do
      it "assigns a new participant as @participant" do
        get :new, {}, valid_session
        expect(assigns(:participant)).to be_a_new(Participant)
      end
    end

    describe "GET edit" do
      it "assigns the requested participant as @participant" do
        get :edit, { id: subject.to_param }, valid_session
        expect(assigns(:participant)).to eq(subject)
      end
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new participant" do
        expect {
          post :create, { participant: valid_attributes }, valid_session
        }.to change(Participant, :count).by(1)
      end

      it "assigns a newly created participant as @participant" do
        post :create, { participant: valid_attributes }, valid_session
        expect(assigns(:participant)).to be_a(Participant)
        expect(assigns(:participant)).to be_persisted
      end

      it "redirects to the created participant" do
        post :create, { participant: valid_attributes }, valid_session
        expect(response).to redirect_to(Participant.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved participant as @participant" do
        post :create, { participant: invalid_attributes }, valid_session
        expect(assigns(:participant)).to be_a_new(Participant)
      end

      it "re-renders the 'new' template" do
        post :create, { participant: invalid_attributes }, valid_session
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT update" do
    before { request.env["HTTP_REFERER"] = "back" }
    
    describe "with valid params" do
      let(:new_attributes) { { name: 'Karina', city: 'Lorem' } }
      let(:registrations_attributes) { new_attributes.merge({ registrations_attributes: [ event_id: 1, group_id: 1 ] }) }

      it "updates the requested participant" do
        put :update, { id: subject.to_param, participant: new_attributes }, valid_session
        subject.reload
        expect(subject.city).to eq('Lorem')
      end

      it "create a registration assigning to a event" do
        put :update, { id: subject.to_param, participant: registrations_attributes }, valid_session
        expect(subject.registrations).to be_any
      end

      it "assigns the requested participant as @participant" do
        put :update, { id: subject.to_param, participant: valid_attributes }, valid_session
        expect(assigns(:participant)).to eq(subject)
      end

      it "redirects to the participant" do
        put :update, { id: subject.to_param, participant: valid_attributes }, valid_session
        expect(response).to redirect_to("back")
      end

    end

    describe "with invalid params" do
      it "assigns the participant as @participant" do
        put :update, { id: subject.to_param, participant: invalid_attributes }, valid_session
        expect(assigns(:participant)).to eq(subject)
      end

      it "re-renders the 'edit' template" do
        put :update, { id: subject.to_param, participant: invalid_attributes }, valid_session
        expect(response).to redirect_to("back")
      end
    end
  end

  describe "DELETE destroy" do
    let(:subject) { FactoryGirl.create :participant }

    it "destroys the requested participant" do
      expect {
        delete :destroy, { id: subject.to_param }, valid_session
      }.to change(Participant, :count).by(0)
    end

    it "redirects to the participants list" do
      delete :destroy, { id: subject.to_param }, valid_session
      expect(response).to redirect_to(participants_url)
    end
  end

end
