require 'rails_helper'

RSpec.describe ManagersController, type: :controller do
  let(:subject) { FactoryGirl.create :manager }
  let(:customer) { subject.customer }
  
  let(:user) { FactoryGirl.create :user }
  let(:valid_session) { sign_in user }

  describe "DELETE destroy" do
    before { request.env["HTTP_REFERER"] = "back" }

    it "destroys the requested manager" do
      expect {
        delete :destroy, { id: subject.to_param }, valid_session
      }.to change(Manager, :count).by(0)
    end

    it "redirects to the customer view" do
      delete :destroy, { id: subject.to_param }, valid_session
      expect(response).to redirect_to("back")
    end
  end

end
