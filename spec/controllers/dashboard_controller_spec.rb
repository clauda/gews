require 'rails_helper'

RSpec.describe DashboardController, type: :controller do

  describe "GET index no authorized" do
    it "returns http redirect" do
      get :index
      expect(response).to be_redirect
    end
  end

  describe "GET index authenticated" do
    let(:user) { FactoryGirl.create :user }
    before { sign_in user }
    it "returns http success" do
      get :index
      expect(response).to be_success
    end
  end

end
