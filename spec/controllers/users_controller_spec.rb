require 'rails_helper'

RSpec.describe UsersController, type: :controller do

  context "GET index" do

    describe "admin role" do
      let(:user) { FactoryGirl.create :user }
      before { sign_in user }
      it "returns http success" do
        get :index
        expect(response).to be_success
      end
    end

    describe "zezinho role" do
      let(:user) { FactoryGirl.create :user, role: 'zezinho' }
      before { sign_in user }
      it "returns http redirected" do
        get :index
        expect(response).to be_redirect
      end
    end

  end

end
