require 'rails_helper'

RSpec.describe EventsController, type: :controller do
  let(:user) { FactoryGirl.create :user }

  let(:valid_attributes) { 
    { 
      name: "Lorem Ipsum", 
      start_at: Date.today, 
      ends_at: Date.tomorrow, 
      local_id: 1,
      customer_id: 1
    } }
  let(:invalid_attributes) { { name: "Cool", local_id: nil } }
  let(:valid_session) { sign_in user }

  describe "GETs" do
    let(:subject) { FactoryGirl.create :event }

    describe "GET index" do
      it "assigns all events as @events" do
        get :index, {}, valid_session
        expect(assigns(:events)).to eq([subject])
      end
    end

    describe "GET show" do
      it "assigns the requested event as @event" do
        get :show, { id: subject.to_param }, valid_session
        expect(assigns(:event)).to eq(subject)
      end
    end

    describe "GET new" do
      it "assigns a new event as @event" do
        get :new, {}, valid_session
        expect(assigns(:event)).to be_a_new(Event)
      end
    end

    describe "GET edit" do
      it "assigns the requested event as @event" do
        get :edit, { id: subject.to_param }, valid_session
        expect(assigns(:event)).to eq(subject)
      end
    end

    describe "GET printer" do
      it "assigns the registrations from event to @registrations" do
        get :printer, { id: subject.to_param }, valid_session
        expect(assigns(:registrations)).to eq(subject.registrations.confirmed)
      end
    end

    describe "GET printer in CSV format" do
      it "assigns the registrations from event to @registrations" do
        get :printer, { id: subject.to_param, format: :csv }, valid_session
        expect(response.content_type).to eq('text/csv')
      end
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new event" do
        expect {
          post :create, { event: valid_attributes }, valid_session
        }.to change(Event, :count).by(1)
      end

      it "assigns a newly created event as @event" do
        post :create, { event: valid_attributes }, valid_session
        expect(assigns(:event)).to be_a(Event)
        expect(assigns(:event)).to be_persisted
      end

      it "redirects to the created event" do
        post :create, { event: valid_attributes }, valid_session
        expect(response).to redirect_to(Event.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved event as @event" do
        post :create, { event: invalid_attributes }, valid_session
        expect(assigns(:event)).to be_a_new(Event)
      end

      it "re-renders the 'new' template" do
        post :create, { event: invalid_attributes }, valid_session
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT update" do
    let(:subject) { FactoryGirl.create :event }
    
    describe "with valid params" do
      let(:new_attributes) { { name: 'Karina', rooms_attributes: [ name: "Sala Rosa", max_capacity: 10 ] } }

      it "updates the requested event" do
        put :update, { id: subject.to_param, event: new_attributes }, valid_session
        subject.reload
        expect(subject.local.city).to eq('Natal')
      end

      it "assigns the requested event as @event" do
        put :update, { id: subject.to_param, event: valid_attributes }, valid_session
        expect(assigns(:event)).to eq(subject)
      end

      it "redirects to the event" do
        put :update, { id: subject.to_param, event: valid_attributes }, valid_session
        subject.reload
        expect(response).to redirect_to(subject)
      end

      it "create a room assings to event" do
        put :update, { id: subject.to_param, event: new_attributes }, valid_session
        expect(subject.rooms.size).to eq(1)
      end
    end

    describe "with invalid params" do
      it "assigns the event as @event" do
        put :update, { id: subject.to_param, event: invalid_attributes }, valid_session
        expect(assigns(:event)).to eq(subject)
      end

      it "re-renders the 'edit' template" do
        put :update, { id: subject.to_param, event: invalid_attributes }, valid_session
        expect(response).to render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    let(:subject) { FactoryGirl.create :event }

    it "destroys the requested event" do
      expect {
        delete :destroy, { id: subject.to_param }, valid_session
      }.to change(Event, :count).by(0)
    end

    it "redirects to the events list" do
      delete :destroy, { id: subject.to_param }, valid_session
      expect(response).to redirect_to(events_url)
    end
  end

end
