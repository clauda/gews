require 'rails_helper'

RSpec.describe RoomsController, type: :controller do
  let(:user) { FactoryGirl.create :user }

  # This should return the minimal set of attributes required to create a valid
  # Room. As you add validations to Room, be sure to
  # adjust the attributes here as well.
  let(:event) { FactoryGirl.create :event }
  let(:room){ FactoryGirl.create(:room) }

  let(:valid_attributes) {
    { name: "Cool", event_id: event.id, max_capacity: 20 }
  }

  let(:invalid_attributes) {
    { name: "Not Cool", min_capacity: 20, event_id: nil }
  }

  let(:valid_session) { sign_in user }

  describe "GET index" do
    it "assigns all rooms as @rooms" do
      get :index, {}, valid_session
      expect(assigns(:rooms)).to eq([room])
    end
  end

  describe "GET show" do
    it "assigns the requested room as @room" do
      get :show, { id: room.to_param }, valid_session
      expect(assigns(:room)).to eq(room)
    end
  end

  describe "GET new" do
    it "assigns a new room as @room" do
      get :new, {}, valid_session
      expect(assigns(:room)).to be_a_new(Room)
    end
  end

  describe "GET edit" do
    it "assigns the requested room as @room" do
      get :edit, { id: room.to_param }, valid_session
      expect(assigns(:room)).to eq(room)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new Room" do
        expect {
          post :create, { room: valid_attributes }, valid_session
        }.to change(Room, :count).by(1)
      end

      it "assigns a newly created room as @room" do
        post :create, { room: valid_attributes }, valid_session
        expect(assigns(:room)).to be_a(Room)
        expect(assigns(:room)).to be_persisted
      end

      it "redirects to the created room" do
        post :create, { room: valid_attributes }, valid_session
        expect(response).to redirect_to(Room.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved room as @room" do
        post :create, { room: invalid_attributes }, valid_session
        expect(assigns(:room)).to be_a_new(Room)
      end

      it "re-renders the 'new' template" do
        post :create, { room: invalid_attributes }, valid_session
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT update" do

    describe "with valid params" do
      let(:new_attributes) { { min_capacity: 2, observations: 'Lorem' } }

      it "updates the requested room" do
        put :update, { id: room.to_param, room: new_attributes}, valid_session
        expect(room.min_capacity).not_to be_nil
      end

      it "assigns the requested room as @room" do
        put :update, { id: room.to_param, room: valid_attributes }, valid_session
        expect(assigns(:room)).to eq(room)
      end

      it "redirects to the room" do
        put :update, { id: room.to_param, room: valid_attributes }, valid_session
        expect(response).to redirect_to(room)
      end
    end

    describe "with invalid params" do
      it "assigns the room as @room" do
        put :update, { id: room.to_param, room: valid_attributes }, valid_session
        expect(assigns(:room)).to eq(room)
      end

      it "re-renders the 'edit' template" do
        put :update, { id: room.to_param, room: invalid_attributes }, valid_session
        expect(response).to render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    before { request.env["HTTP_REFERER"] = "back" }

    it "destroys the requested room" do
      expect {
        delete :destroy, { id: room.to_param }, valid_session
      }.to change(Room, :count).by(0)
    end

    it "redirects to the rooms list" do
      delete :destroy, { id: room.to_param }, valid_session
      expect(response).to redirect_to("back")
    end
  end

end
