require 'rails_helper'

RSpec.describe CustomersController, type: :controller do
  let(:user) { FactoryGirl.create :user }
  let(:subject) { FactoryGirl.create :customer }

  let(:valid_attributes) { { name: "Natura", city: "São Paulo", state: "SP" } }
  let(:invalid_attributes) { { name: "Cool", city: nil, state: nil } }
  let(:valid_session) { sign_in user }

  describe "GET index" do
    it "assigns all customers as @customers" do
      get :index, {}, valid_session
      expect(assigns(:customers)).to eq([subject])
    end
  end

  describe "GET show" do
    it "assigns the requested customer as @customer" do
      get :show, { id: subject.to_param }, valid_session
      expect(assigns(:customer)).to eq(subject)
    end
  end

  describe "GET new" do
    it "assigns a new customer as @customer" do
      get :new, {}, valid_session
      expect(assigns(:customer)).to be_a_new(Customer)
    end
  end

  describe "GET edit" do
    it "assigns the requested customer as @customer" do
      get :edit, { id: subject.to_param }, valid_session
      expect(assigns(:customer)).to eq(subject)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new customer" do
        expect {
          post :create, { customer: valid_attributes }, valid_session
        }.to change(Customer, :count).by(1)
      end

      it "assigns a newly created customer as @customer" do
        post :create, { customer: valid_attributes }, valid_session
        expect(assigns(:customer)).to be_a(Customer)
        expect(assigns(:customer)).to be_persisted
      end

      it "redirects to the created customer" do
        post :create, { customer: valid_attributes }, valid_session
        expect(response).to redirect_to(Customer.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved customer as @customer" do
        post :create, { customer: invalid_attributes }, valid_session
        expect(assigns(:customer)).to be_a_new(Customer)
      end

      it "re-renders the 'new' template" do
        post :create, { customer: invalid_attributes }, valid_session
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      let(:new_attributes) { { name: 'Karina', city: 'Lorem' } }

      it "updates the requested customer" do
        put :update, { id: subject.to_param, customer: new_attributes }, valid_session
        subject.reload
        expect(subject.city).to eq('Lorem')
      end

      it "assigns the requested customer as @customer" do
        put :update, { id: subject.to_param, customer: valid_attributes }, valid_session
        expect(assigns(:customer)).to eq(subject)
      end

      it "redirects to the customer" do
        put :update, { id: subject.to_param, customer: valid_attributes }, valid_session
        expect(response).to redirect_to(subject)
      end
    end

    describe "with invalid params" do
      it "assigns the customer as @customer" do
        put :update, { id: subject.to_param, customer: invalid_attributes }, valid_session
        expect(assigns(:customer)).to eq(subject)
      end

      it "re-renders the 'edit' template" do
        put :update, { id: subject.to_param, customer: invalid_attributes }, valid_session
        expect(response).to render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested customer" do
      expect {
        delete :destroy, { id: subject.to_param }, valid_session
      }.to change(Customer, :count).by(0)
    end

    it "redirects to the customers list" do
      delete :destroy, { id: subject.to_param }, valid_session
      expect(response).to redirect_to(customers_url)
    end
  end

end
