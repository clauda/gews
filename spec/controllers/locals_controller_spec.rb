require 'rails_helper'

RSpec.describe LocalsController, type: :controller do
  let(:user) { FactoryGirl.create :user }

  let(:valid_attributes) { { name: "Casa da Mãe Joana", city: "Natal", state: "RN" } }
  let(:invalid_attributes) { { name: nil, city: nil, state: nil } }
  let(:valid_session) { sign_in user }

  describe "GETs" do
    let(:subject) { FactoryGirl.create :local }

    describe "GET index" do
      it "assigns all local as @locals" do
        get :index, {}, valid_session
        expect(assigns(:locals)).to eq([subject])
      end
    end

    describe "GET show" do
      it "assigns the requested local as @local" do
        get :show, { id: subject.to_param }, valid_session
        expect(assigns(:local)).to eq(subject)
      end
    end

    describe "GET new" do
      it "assigns a new local as @local" do
        get :new, {}, valid_session
        expect(assigns(:local)).to be_a_new(Local)
      end
    end

    describe "GET edit" do
      it "assigns the requested local as @local" do
        get :edit, { id: subject.to_param }, valid_session
        expect(assigns(:local)).to eq(subject)
      end
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new local" do
        expect {
          post :create, { local: valid_attributes }, valid_session
        }.to change(Local, :count).by(1)
      end

      it "assigns a newly created local as @local" do
        post :create, { local: valid_attributes }, valid_session
        expect(assigns(:local)).to be_a(Local)
        expect(assigns(:local)).to be_persisted
      end

      it "redirects to the created local" do
        post :create, { local: valid_attributes }, valid_session
        expect(response).to redirect_to(Local.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved local as @local" do
        post :create, { local: invalid_attributes }, valid_session
        expect(assigns(:local)).to be_a_new(Local)
      end

      it "re-renders the 'new' template" do
        post :create, { local: invalid_attributes }, valid_session
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT update" do
    let(:subject) { FactoryGirl.create :local }
    
    describe "with valid params" do
      let(:new_attributes) { { name: 'Ibira', city: 'São Paulo', state: 'SP' } }

      it "updates the requested local" do
        put :update, { id: subject.to_param, local: new_attributes }, valid_session
        subject.reload
        expect(subject.city).to eq('São Paulo')
      end

      it "assigns the requested local as @local" do
        put :update, { id: subject.to_param, local: valid_attributes }, valid_session
        expect(assigns(:local)).to eq(subject)
      end

      it "redirects to the local" do
        put :update, { id: subject.to_param, local: valid_attributes }, valid_session
        subject.reload
        expect(response).to redirect_to(subject)
      end
    end

    describe "with invalid params" do
      it "assigns the local as @local" do
        put :update, { id: subject.to_param, local: invalid_attributes }, valid_session
        expect(assigns(:local)).to eq(subject)
      end

      it "re-renders the 'edit' template" do
        put :update, { id: subject.to_param, local: invalid_attributes }, valid_session
        expect(response).to render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    let(:subject) { FactoryGirl.create :local }

    it "destroys the requested event" do
      expect {
        delete :destroy, { id: subject.to_param }, valid_session
      }.to change(Local, :count).by(0)
    end

    it "redirects to the events list" do
      delete :destroy, { id: subject.to_param }, valid_session
      expect(response).to redirect_to(locals_url)
    end
  end

end
