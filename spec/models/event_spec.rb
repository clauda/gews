require 'rails_helper'

RSpec.describe User, type: :model do
  subject { FactoryGirl.create :event, start_at: Date.tomorrow.to_time, ends_at: Date.today + 2 }
  it { expect(subject).to be_valid }

  describe ".in_coming events" do
    let!(:soon) { Event.in_coming }
    before { subject.reload } # < damn its

    it { expect(soon).not_to be_empty }
    it { expect(soon.ids).to include(subject.id) }
    it { expect(soon.first.start_at).to be_future }
  end
end