require 'rails_helper'

RSpec.describe User do
  subject { FactoryGirl.create :user }
  it { expect(subject).to be_valid }
  it { expect(subject).to be_is_god }
end