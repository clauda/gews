require 'rails_helper'

RSpec.describe Registration, type: :model do
  subject { FactoryGirl.create :registration, confirm: true }
  it { expect(subject).to be_valid }
  it { expect(subject).to respond_to(:name) }
  it { expect(subject).to respond_to(:email) }
  it { expect(subject).to respond_to(:phone) }
  it { expect(subject).to respond_to(:cpf) }

  describe ".confirmed" do
    before { subject.reload } #stupid
    it { expect(Registration.confirmed.first).to be_confirm }
    it { expect(Registration.confirmed.ids).to include(subject.id) }
  end
end
