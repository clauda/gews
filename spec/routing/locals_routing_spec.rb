require "rails_helper"

RSpec.describe LocalsController, type: :routing do
  describe "routing" do
    it ("routes to #index") { expect(get: "/estabelecimentos").to route_to("locals#index") }
    it ("routes to #show") { expect(get: "/estabelecimentos/1").to route_to("locals#show", id: "1") }
    it ("routes to #create") { expect(post: "/estabelecimentos").to route_to("locals#create") }
    it ("routes to #update") { expect(put: "/estabelecimentos/1").to route_to("locals#update", id: "1") }
    it ("routes to #destroy") { expect(delete: "/estabelecimentos/1").to route_to("locals#destroy", id: "1") }
  end
end
