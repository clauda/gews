require "rails_helper"

RSpec.describe EventsController, type: :routing do
  describe "routing" do
    it ("routes to #index") { expect(get: "/eventos").to route_to("events#index") }
    it ("routes to #show") { expect(get: "/eventos/1").to route_to("events#show", id: "1") }
    it ("routes to #create") { expect(post: "/eventos").to route_to("events#create") }
    it ("routes to #update") { expect(put: "/eventos/1").to route_to("events#update", id: "1") }
    it ("routes to #destroy") { expect(delete: "/eventos/1").to route_to("events#destroy", id: "1") }

    it ("routes to #registrations") { expect(get: "/eventos/1/inscritos").to route_to("events#registrations", id: "1") }
    it ("routes to #import") { expect(patch: "/eventos/1/import").to route_to("events#import", id: "1") }
  end
end
