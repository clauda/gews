require "rails_helper"

RSpec.describe ManagersController, type: :routing do
  describe "routing" do
    it "routes to #destroy" do
      expect(:delete => "/managers/1").to route_to("managers#destroy", id: "1")
    end
  end
end
