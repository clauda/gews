require "rails_helper"

RSpec.describe RegistrationsController, type: :routing do
  describe "routing" do
    it "routes to #destroy" do
      expect(:delete => "/registrations/1").to route_to("registrations#destroy", id: "1")
    end
  end
end
