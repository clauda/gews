require "rails_helper"

RSpec.describe CustomersController, :type => :routing do
  describe "routing" do
    it ("routes to #index") { expect(get: "/clientes").to route_to("customers#index") }
    it ("routes to #show") { expect(get: "/clientes/1").to route_to("customers#show", id: "1") }
    it ("routes to #create") { expect(post: "/clientes").to route_to("customers#create") }
    it ("routes to #update") { expect(put: "/clientes/1").to route_to("customers#update", id: "1") }
    it ("routes to #destroy") { expect(delete: "/clientes/1").to route_to("customers#destroy", id: "1") }
  end
end

