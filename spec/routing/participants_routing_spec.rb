require "rails_helper"

RSpec.describe ParticipantsController, type: :routing do
  describe "routing" do
    it ("routes to #index") { expect(get: "/participantes").to route_to("participants#index") }
    it ("routes to #show") { expect(get: "/participantes/1").to route_to("participants#show", id: "1") }
    it ("routes to #create") { expect(post: "/participantes").to route_to("participants#create") }
    it ("routes to #update") { expect(put: "/participantes/1").to route_to("participants#update", id: "1") }
    it ("routes to #destroy") { expect(delete: "/participantes/1").to route_to("participants#destroy", id: "1") }
  end
end
