require "rails_helper"

RSpec.describe RoomsController, type: :routing do
  describe "routing" do
    it ("routes to #index") { expect(get: "/salas").to route_to("rooms#index") }
    it ("routes to #show") { expect(get: "/salas/1").to route_to("rooms#show", id: "1") }
    it ("routes to #create") { expect(post: "/salas").to route_to("rooms#create") }
    it ("routes to #update") { expect(put: "/salas/1").to route_to("rooms#update", id: "1") }
    it ("routes to #destroy") { expect(delete: "/salas/1").to route_to("rooms#destroy", id: "1") }
  end
end
