require "rails_helper"

RSpec.describe GroupsController, type: :routing do
  describe "routing" do
    it ("routes to #index") { expect(get: "/grupos").to route_to("groups#index") }
    it ("routes to #show") { expect(get: "/grupos/1").to route_to("groups#show", id: "1") }
    it ("routes to #create") { expect(post: "/grupos").to route_to("groups#create") }
    it ("routes to #update") { expect(put: "/grupos/1").to route_to("groups#update", id: "1") }
    it ("routes to #destroy") { expect(delete: "/grupos/1").to route_to("groups#destroy", id: "1") }
  end
end
