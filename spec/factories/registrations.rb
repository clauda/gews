FactoryGirl.define do
  factory :registration do
    participant
    event
    group
    confirm false
    check_in_at "2014-11-23 19:19:43"
    check_out_at "2014-11-23 19:19:43"
    medicals "Lorem ipsum dolor sit amet, consectetur adipisicing elit."
    refund "90,0"
    transportation "Bus"
    observations "Lorem..."
  end
end
