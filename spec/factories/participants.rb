FactoryGirl.define do
  factory :participant do
    name "Marcelo"
    cno_code 198
    city "São Paulo"
    state "SP"
    document 01077755500
    address "Rua dos Bobos, 0"
    phone "11 99999999"
    email "email@email.com"
  end
end
