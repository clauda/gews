FactoryGirl.define do
  factory :room do
    event
    name "Damn Event"
    max_capacity 100
    min_capacity 1
    observations "Lorem ipsum dolor sit amet, consectetur adipisicing elit."
  end
end
