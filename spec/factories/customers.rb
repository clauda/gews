FactoryGirl.define do
  factory :customer do
    name "Natura"
    city "Natal"
    state "RN"
  end
end
