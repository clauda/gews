FactoryGirl.define do
  factory :user do
    name 'Administrador'
    email 'admin@email.com'
    password 'senhasecreta'
    role 'admin'
  end
end
