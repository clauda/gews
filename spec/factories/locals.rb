FactoryGirl.define do
  factory :local do
    name "Centro de Convenções"
    city "Natal"
    state "RN"
    address "Rua dos Bobos"
    country "Brasil"
    neighborhood "Bela Vista"
    number "0"
    zip "3128010"
    phone "11 95555 55555"
    email "email@email.com"
  end
end
