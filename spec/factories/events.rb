FactoryGirl.define do
  factory :event do
    customer
    local
    name "WorkShop Whatever"
    kind "workshop"
    manager "Zezinho"
    start_at "2014-10-21 17:13:15"
    ends_at "2014-10-21 17:13:15"
    montage_date "2014-10-21 17:13:15"
    montage_ends "2014-10-22 17:13:15"
    phone "84 9999 9999"
    observations "Lorem ipsum dolor sit amet, consectetur adipisicing elit."
    internals "Lorem ipsum dolor sit amet, consectetur adipisicing elit."
    max_clients 100
  end
end
