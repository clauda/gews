class RemoveColumnLocalNameFromEvent < ActiveRecord::Migration
  def change
    remove_column :events, :local_name, :string
    remove_column :events, :city, :string
    remove_column :events, :state, :string
    remove_column :events, :address, :string
    remove_column :events, :number, :string
    remove_column :events, :cep, :string
    remove_column :events, :neighborhood, :string
  end
end
