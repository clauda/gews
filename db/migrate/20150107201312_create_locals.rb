class CreateLocals < ActiveRecord::Migration
  def change
    create_table :locals do |t|
      t.string :name
      t.string :city
      t.string :state
      t.string :address
      t.string :country
      t.string :neighborhood
      t.string :number
      t.string :zip
      t.string :phone
      t.string :email

      t.timestamps
    end
  end
end
