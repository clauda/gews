class CreateManagers < ActiveRecord::Migration
  def change
    create_table :managers do |t|
      t.string :name
      t.references :customer, index: true

      t.timestamps
    end
  end
end
