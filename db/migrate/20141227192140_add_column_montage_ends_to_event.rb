class AddColumnMontageEndsToEvent < ActiveRecord::Migration
  def change
    add_column :events, :montage_ends, :datetime
  end
end
