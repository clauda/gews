class CreateRegistrations < ActiveRecord::Migration
  def change
    create_table :registrations do |t|
      t.references :participant, index: true
      t.references :event, index: true
      t.references :group, index: true
      t.boolean :confirm, default: false
      t.datetime :check_in_at
      t.datetime :check_out_at
      t.text :medicals
      t.string :refund
      t.string :transportation
      t.text :observations

      t.timestamps
    end
  end
end
