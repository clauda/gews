class AddColumnSheetToEvent < ActiveRecord::Migration
  def change
    add_column :events, :sheet, :string
  end
end
