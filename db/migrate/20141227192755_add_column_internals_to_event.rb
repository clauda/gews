class AddColumnInternalsToEvent < ActiveRecord::Migration
  def change
    add_column :events, :internals, :text
  end
end
