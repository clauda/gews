class CreateRooms < ActiveRecord::Migration
  def change
    create_table :rooms do |t|
      t.references :event, index: true
      t.string :name
      t.integer :max_capacity
      t.integer :min_capacity
      t.text :observations

      t.timestamps
    end
  end
end
