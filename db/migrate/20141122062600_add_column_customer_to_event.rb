class AddColumnCustomerToEvent < ActiveRecord::Migration
  def change
    add_reference :events, :customer, index: true
  end
end
