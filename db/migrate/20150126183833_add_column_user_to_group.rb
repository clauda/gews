class AddColumnUserToGroup < ActiveRecord::Migration
  def change
    add_reference :groups, :user, index: true
    add_foreign_key :groups, :users
    remove_column :groups, :manager, :string
  end
end
