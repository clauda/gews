class AddColumnLocalToEvent < ActiveRecord::Migration
  def change
    add_reference :events, :local, index: true
  end
end
