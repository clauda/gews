class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.string :kind
      t.string :manager
      t.datetime :start_at
      t.datetime :ends_at
      t.datetime :montage_date
      t.string :local_name
      t.string :city
      t.string :state
      t.string :address
      t.string :number
      t.string :cep
      t.string :neighborhood
      t.string :phone
      t.text :observations
      t.integer :max_clients

      t.timestamps
    end
  end
end
