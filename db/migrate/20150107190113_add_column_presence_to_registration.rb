class AddColumnPresenceToRegistration < ActiveRecord::Migration
  def change
    add_column :registrations, :presence, :boolean, default: false
  end
end
