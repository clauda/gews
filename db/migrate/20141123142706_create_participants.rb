class CreateParticipants < ActiveRecord::Migration
  def change
    create_table :participants do |t|
      t.string :name
      t.integer :cno_code
      t.string :city
      t.string :state
      t.string :document
      t.string :address
      t.string :phone
      t.string :email

      t.timestamps
    end
  end
end
