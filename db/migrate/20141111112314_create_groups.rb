class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.string :name
      t.string :region
      t.string :manager
      t.string :sector

      t.timestamps
    end
  end
end
