# encoding: utf-8

module Importable
  class RegistrationsImporter < ActiveImporter::Base
    imports Registration
    transactional

    attr_reader :row_count

    # column 'GRUPO_CNO', :group_cno, optional: true
    
    on :row_processing do
      model.event_id = params[:event_id]

      group = Group.find_or_create_by(name: row['NOME_SETOR'])
      group.update_column(:user_id, find_manager(row['RESPONSAVEL_SETOR']))

      model.group_id = group.id

      participant = Participant.find_or_create_by(name: row['NOME_CNO'])
      participant.cno_code = row['COD_CNO'] 
      participant.city = row['CIDADE']
      participant.state = row['ESTADO']
      participant.save

      model.participant_id = participant.id
    end

    on :import_started do
      @row_count = 0
    end

    on :row_processed do
      @row_count += 1
    end

    on :import_finished do
      notify("#{@row_count} imported successfully!")
    end

    on :import_failed do |exception|
      notify("Fatal error while importing data: #{exception.message}")
    end

    private

      def find_manager name
        manager = User.find_by(name: name)
        if manager.nil?
          manager = User.create(name: name, email: "#{dotenize(name)}@fabrica2.com.br", password: "q1w2e3r4", password_confirmation: "q1w2e3r4", role: 'zezinho')
        end
        manager.id
      end

      def notify message
        p message
      end

      def dotenize name
        name.gsub(' ', '.').downcase
      end

  end
end