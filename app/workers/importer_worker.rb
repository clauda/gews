class ImporterWorker
  include Sidekiq::Worker
  sidekiq_options :queue => :crawler, :retry => false, :backtrace => true

  def perform event_id
    resource = Event.find event_id
    Importable::RegistrationsImporter.import(resource.sheet.url, params: { event_id: resource.id })
  end
end