# encoding: utf-8

class SheetUploader < CarrierWave::Uploader::Base

  # Choose what kind of storage to use for this uploader:
  storage :fog

  after :store, :import!

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "gews/#{model.id}"
  end

  def extension_white_list
    %w(cvs xls xlsx)
  end

  def import! file
    ImporterWorker.perform_async(model.id)
  end

end
