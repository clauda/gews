GEWS.Events =

  list: ->
    if (!cache['events']) then this._fetch()
    cache.events

  calendar: ->
    jQuery('#full-clndr').clndr
      template: jQuery('#full-clndr-template').html()
      daysOfTheWeek: [ "Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb" ]
      events: cache.events
      multiDayEvents:
        startDate: 'start_at'
        endDate: 'ends_at'

  tips: ->
    Tipped.create ".tipped", (->
      title: jQuery(this).data("title")
      content: jQuery(this).data("content")
    ),
      size: "large"
      radius: false
      position: "topleft"


  _fetch: ->
    jQuery.getJSON '/events.json', (json)->
      cache['events'] = json
    return