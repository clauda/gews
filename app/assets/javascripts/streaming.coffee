class GEWS.Streaming

  constructor: (@source)->
    _self = @
    @eventSource = new EventSource(@source)
    @eventSource.addEventListener 'importer.successful', _self.importDone, true
    
    setInterval(->
      jQuery('.notification').text('Isso pode levar alguns minutos...').removeClass('info').addClass('error')
      _self._finish()
    , 8000)

  importDone: (e)=>
    data = JSON.parse(e.data)
    console.log 'listerning'
    if data.message == "done"
      jQuery('.notification').text('Importação completa!').addClass('notice')
      location.reload()

      @_finish()
    false


  _finish: ->
    @eventSource.close()
    @