//
//= require jquery-ui
//= require underscore-min
//= require jquery.formalize.min
//= require jquery.validate
//= require jquery.checkboxes
//= require chosen.jquery.min
//= require jquery.livequery
//= require jquery.datetimepicker
//= require moment.min
//= require tipped
//= require clndr
//= require switchery
//

_.templateSettings = {
  interpolate: /\{\{\=(.+?)\}\}/g,
  escape: /\{\{\-(.+?)\}\}/g,
  evaluate: /\{\{(.+?)\}\}/g
};