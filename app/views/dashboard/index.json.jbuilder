json.array!(@events) do |event|
  json.start_at date_for(event.start_at)
  json.ends_at date_for(event.ends_at)
  json.title event.name
  json.location event.local.name
  json.url event_url(event)
  json.is_current is_current?(event.start_at, event.ends_at) ? "current" : "old"
  json.content "#{event.local.name} <br> #{event.kind} <br> #{event.registrations.size} participantes"
end