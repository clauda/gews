module ApplicationHelper

  def nav_for controller
    if %w(users registrations).member? controller
      render 'commons/nav_users'
    elsif %w(customers locals managers participants).member? controller
      render 'commons/nav_customers'
    else
      render 'commons/nav_events'
    end
  end

  def datetime_for datetime
    datetime.strftime '%d/%m/%Y às %H:%M' if datetime
  end

  def date_for datetime
    datetime.strftime '%Y-%m-%d' if datetime
  end

  STATES = %w(AC AL AM AP BA CE DF ES GO MA MG MS MT PA PB PE PI PR RJ RN RR RO RS SC SE SP TO)

  # Overwrite
  # change the default link renderer for will_paginate
  def will_paginate(collection_or_options = nil, options = {})
    if collection_or_options.is_a? Hash
      options, collection_or_options = collection_or_options, nil
    end
    unless options[:renderer]
      options = options.merge renderer: PaginationHelper::LinkRenderer
    end
    super *[collection_or_options, options].compact
  end

end
