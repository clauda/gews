module EventsHelper

  def is_current? start_at, ends_at
    (start_at.past? || start_at.today?) && ends_at.future?
  end

end
