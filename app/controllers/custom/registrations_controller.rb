class Custom::RegistrationsController < Devise::RegistrationsController
  prepend_before_filter :require_no_authentication, only: [ :new, :create, :cancel ]

  def create
    resource = User.new user_params
    @user = resource

    if resource.save
      redirect_to user_path(resource), notice: 'Usuário criado com sucesso.'
    else
      clean_up_passwords resource
      redirect_to new_user_path(resource), notice: resource.errors.full_messages
    end
  end

  def update
    account_update_params = devise_parameter_sanitizer.sanitize(:account_update)

    # required for settings form to submit when password is left blank
    if account_update_params[:password].blank?
      account_update_params.delete("password")
      account_update_params.delete("password_confirmation")
    end

    if resource.update_attributes(account_update_params)
      set_flash_message :notice, :updated
      # Sign in the user bypassing validation in case his password changed
      # sign_in @user, :bypass => true
      redirect_to :back
    else
      redirect_to :back, alert: resource.errors.full_messages
    end
  end

  private

    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation, :address, :city, :state, :role, :phone)
    end

end