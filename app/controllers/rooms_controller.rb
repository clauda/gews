class RoomsController < InheritedResources::Base
  before_action :authenticate_user!
  before_action :denied!

  def destroy
    super do |format|
      format.html { redirect_to :back, notice: 'Sala removida!' }
    end
  end

  private
  
    def collection
      get_collection_ivar || set_collection_ivar(end_of_association_chain.paginate(page: params[:pagina]))
    end

    def room_params
      params.require(:room).permit(:name, :event_id, :max_capacity, :min_capacity, :observations)
    end
end
