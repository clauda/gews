class UsersController < InheritedResources::Base
  before_action :authenticate_user!
  before_action :denied!
  actions :index, :new, :edit, :update, :show

  def update
    if user_params[:password].blank?
      params[:user].delete("password")
      params[:user].delete("password_confirmation")
    end

    if resource.update_attributes(user_params)
      redirect_to :back, notice: 'Atualizado'
    else
      redirect_to :back, alert: resource.errors.full_messages
    end
  end

  private

    def collection
      get_collection_ivar || set_collection_ivar(end_of_association_chain.paginate(page: params[:pagina]))
    end

    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation, :address, :city, :state, :role, :phone)
    end
    
end