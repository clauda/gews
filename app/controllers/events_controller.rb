class EventsController < InheritedResources::Base
  before_action :authenticate_user!
  before_action :denied!

  def registrations
    # Sorry about this
    @cities = resource.registrations.ordered.pluck("participants.city").uniq
    @registrations = resource.registrations.ordered.includes(:participant)
    if params[:cidade] && !params[:cidade].blank?
      @registrations = @registrations.where("participants.city = ?", params[:cidade])
    end
    if params[:confirmados] == "on"
      @registrations = @registrations.confirmed
    end
    if params[:nome] && !params[:nome].blank?
      @registrations = @registrations.where("participants.name LIKE ?", "%#{params[:nome]}%")
    end
    @registrations = @registrations.paginate(page: params[:pagina])
  end

  def import
    resource.sheet = params[:event][:sheet]
    resource.save!
    redirect_to registrations_event_path(resource), notice: 'Processando arquivo...'
  end

  def printer
    @registrations = resource.registrations.confirmed.ordered
    respond_to do |format|
      format.html { render layout: 'printer' }
      format.csv do
        headers['Content-Disposition'] = "attachment; filename=\"lista-#{resource.name.parameterize}\""
        headers['Content-Type'] ||= 'text/csv'
      end
    end
  end

  private

    def collection
      get_collection_ivar || set_collection_ivar(end_of_association_chain.paginate(page: params[:pagina]))
    end

    def event_params
      params.require(:event).permit(:name, :customer_id, :kind, :local_id, :start_at, :ends_at, :manager, :max_clients, :montage_date, :montage_ends, :observations, :internals, :phone, :sheet,
        rooms_attributes: [ :name, :max_capacity, :min_capacity ])
    end

end