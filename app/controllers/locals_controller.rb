class LocalsController < InheritedResources::Base
  before_action :authenticate_user!
  before_action :denied!

  private
  
    def collection
      get_collection_ivar || set_collection_ivar(end_of_association_chain.paginate(page: params[:pagina]))
    end

    def local_params
      params.require(:local).permit(:name, :address, :number, :city, :state, :neighborhood, :country, :phone, :email)
    end
end
