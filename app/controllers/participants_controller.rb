class ParticipantsController < InheritedResources::Base
  before_action :authenticate_user!
  before_action :denied!

  def update
    super do |format|
      if resource.errors.any?
        format.html { redirect_to :back, alert: resource.errors.full_messages }
      else
        format.html { redirect_to :back, notice: 'Atualizado' }
      end
    end
  end

  private

    def collection
      get_collection_ivar || set_collection_ivar(end_of_association_chain.paginate(page: params[:pagina]))
    end

    def participant_params
      params.require(:participant).permit(:name, :cno_code, :city, :state, :document, :address, :email, :phone,
        registrations_attributes: [:event_id, :group_id, :confirm, :check_in_at, :check_out_at, :refund, :transportation, :medicals, :observations])
    end
end
