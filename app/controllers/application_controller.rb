class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :configure_permitted_parameters, if: :devise_controller?
  rescue_from ActionController::RoutingError, with: :render_404

  def after_sign_in_path_for resource
    root_path
  end

  def denied!
    render_403 unless current_user.is_god?
  end

  protected
    # There are just three actions in Devise that allows any set of parameters to be passed down to the model,
    # therefore requiring sanitization. Their names and the permited parameters by default are:

    # sign_in (Devise::SessionsController#new) - Permits only the authentication keys (like email)
    # sign_up (Devise::RegistrationsController#create) - Permits authentication keys plus password and password_confirmation
    # account_update (Devise::RegistrationsController#update) - Permits authentication keys plus password, password_confirmation
    # and current_password. More at https://github.com/plataformatec/devise#strong-parameters

    def configure_permitted_parameters
      devise_parameter_sanitizer.for(:account_update) do |user|
        user.permit(:name, :email, :password, :password_confirmation, :address, :city, :state, :role, :phone)
      end
    end

    def render_404(exception = nil)
      render(file: "#{Rails.root}/public/404.html", status: 404, layout: false)
    end

    def render_403
      redirect_to root_path, error: 'Acesso Negado'
    end

end
