class CustomersController < InheritedResources::Base
  before_action :authenticate_user!
  before_action :denied!

  private

    def collection
      get_collection_ivar || set_collection_ivar(end_of_association_chain.paginate(page: params[:pagina]))
    end

    def customer_params
      params.require(:customer).permit(:name, :city, :state, managers_attributes: [ :name ])
    end
end
