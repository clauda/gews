class DashboardController < ApplicationController
  before_action :authenticate_user!
  respond_to :json, :html

  def index
    @events = Event.all
  end
end
