class RegistrationsController < InheritedResources::Base
  before_action :authenticate_user!
  before_action :denied!
  actions :destroy, :confirm

  def confirm
    resource = Registration.find params[:id]
    resource.toggle! params[:field].to_sym
    render nothing: true
  end

  def destroy
    super do |format|
      format.html { redirect_to :back, notice: 'Inscrição removida!' }
    end
  end

end