class GroupsController < InheritedResources::Base
  before_action :authenticate_user!
  before_action :denied!

  private
  
    def collection
      get_collection_ivar || set_collection_ivar(end_of_association_chain.paginate(page: params[:pagina]))
    end

    def group_params
      params.require(:group).permit(:name, :region, :user_id, :sector)
    end
end

