class ManagersController < InheritedResources::Base
  before_action :authenticate_user!
  before_action :denied!
  actions :destroy

  def destroy
    super do |format|
      format.html { redirect_to :back, notice: 'Gerência removida!' }
    end
  end

end
