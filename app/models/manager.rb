class Manager < ActiveRecord::Base
  belongs_to :customer
  validates :name, :customer_id, presence: true
end
