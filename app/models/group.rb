class Group < ActiveRecord::Base
  belongs_to :user
  validates :name, presence: true

  def manager
    self.user.try :name
  end
end
