class Room < ActiveRecord::Base
  validates :name, :event_id, :max_capacity, presence: true
  belongs_to :event
end
