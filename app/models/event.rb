class Event < ActiveRecord::Base
  default_scope { order(start_at: :desc) }

  belongs_to :customer
  belongs_to :local
  has_many :rooms
  has_many :registrations
  has_many :participants, through: :registrations

  mount_uploader :sheet, SheetUploader

  delegate :address, :number, :cep, :neighborhood, :city, :state, to: :local

  KINDA = [
    [ 'Workshop', 'Workshop'],
    [ 'Seminário', 'Seminário'],
    [ 'Conferência', 'Conferência'],
    [ 'Palestras', 'Palestras'],
    [ 'Convenção', 'Convenção'],
    [ 'Evento Social', 'Evento Social'],
    [ 'Evento Corporativo', 'Evento Corporativo'],
    [ 'Lançamento', 'Lançamento']
  ]

  validates :name, :start_at, :ends_at, :local_id, :customer_id, presence: true

  accepts_nested_attributes_for :rooms

  scope :in_coming, -> { where("ends_at >= ?", Time.zone.now) }

  def to_param
    "#{self.id}-#{self.name.parameterize}"
  end

  def is_current?
    (self.start_at.past? || self.start_at.today?) && self.ends_at.future?
  end

  def past?
    self.ends_at.past?
  end

end
