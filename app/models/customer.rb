class Customer < ActiveRecord::Base
  validates :name, :city, :state, presence: true
  has_many :events
  has_many :managers
  accepts_nested_attributes_for :managers
end
