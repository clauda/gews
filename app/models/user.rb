class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  
  has_many :groups

  # TODO: refactor
  ROLES = {
    admin: 'Administrador',
    zezinho: 'Pode Ler',
    huguinho: 'Pode confirmar todos',
    luizinho: 'Pode confirmar grupo'
  }

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  def is_god?
    self.role == 'admin'
  end

end
