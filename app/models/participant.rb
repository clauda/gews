class Participant < ActiveRecord::Base
  validates :name, presence: true
  has_many :registrations
  has_many :events, through: :registrations

  accepts_nested_attributes_for :registrations
end
