class Local < ActiveRecord::Base
  has_many :events
  validates :name, :city, :state, presence: true
end
