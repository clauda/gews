class Registration < ActiveRecord::Base
  belongs_to :participant
  belongs_to :event
  belongs_to :group

  validates :participant_id, :event_id, :group_id, presence: true
  validates :participant_id, uniqueness: { scope: :event_id }

  delegate :name, :email, :phone, :cpf, :city, :state, :cno_code, to: :participant

  scope :confirmed, -> { where(confirm: true) }
  scope :present, -> { where(presence: true) }
  scope :ordered, -> { includes(:participant).order('participants.name') }
end
