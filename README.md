# App Gerenciador de Eventos

[![Codeship Status for wipcodes/gews](https://www.codeship.io/projects/1ed3aa50-3594-0132-115c-220292a78f73/status)](https://www.codeship.io/projects/41072)

Sistema gerenciador de eventos.

- Linguagem: Ruby 2.2.0
- Framework: Rails 4.2.0
- Banco de Dados: MySQl 5.6.15

## Configurações

Requisitos do sistema:

Bundler: `gem install bundler`

No terminal, execute os seguintes comandos:

`git clone git@bitbucket.org:wipcodes/gews.git` para obter uma cópia do código fonte;

No diretório raiz do projeto, execute:

`bundle install` para instalar as dependências do sistema;

Faça uma cópia do arquivo de configuração exemplo e altere os dados para acesso ao seu banco de dados:

`cp config/database.example.yml config/database.yml` e no arquivo informe usuario e senha do mysql;

`bundle exec rake db:create` para criar o banco de dados;

`bundle exec rake db:setup` para criar as tabelas e popular o banco de dados com dados iniciais;

Para rodar a aplicação:

`rails server`

No navegador: [http://localhost:3000](http://localhost:3000)


