Rails.application.routes.draw do

  devise_for :users, controllers: { registrations: 'custom/registrations' }, skip: [ :sessions ]
  as :user do
    get 'entrar' => "devise/sessions#new", as: :new_user_session
    post 'signin' => 'devise/sessions#create', as: :user_session
    delete 'sair' => 'devise/sessions#destroy', as: :destroy_user_session
  end

  scope(path_names: { new: 'novo', edit: 'editar' }) do
    resources :users, except: [ :create ], path: '/usuarios'
    resources :events, path: '/eventos' do
      get 'inscritos', on: :member, action: :registrations, as: :registrations
      patch :import, on: :member
      get :lista, on: :member, action: :printer, as: :printer
    end
    resources :locals, path: '/estabelecimentos'
    resources :rooms, path: '/salas'
    resources :groups, path: '/grupos'
    resources :customers, path: '/clientes'
    resources :managers, only: :destroy
    resources :participants, path: '/participantes'
    resources :registrations, only: :destroy do
      post :confirm, on: :collection
    end
  end

  get "events", to: "dashboard#index", format: true

  root to: "dashboard#index"
end
